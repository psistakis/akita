module gitlab.com/akita/akita/v2

require (
	github.com/golang/mock v1.4.4
	github.com/gorilla/mux v1.8.0
	github.com/onsi/ginkgo v1.15.0
	github.com/onsi/gomega v1.10.5
	github.com/rs/xid v1.2.1
	github.com/syifan/goseth v0.0.4
)

// replace github.com/syifan/goseth => ../goseth

go 1.16
